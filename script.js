

// // user input
// var balance = prompt("Enter account balance:");
// var limit = prompt("Enter spending limit:");
// const mobilePrice = prompt("Enter mobile price:");
// const gSTtax = prompt("Enter GST value in percentage:");
// // // const accessoriesPrice = prompt("Enter accessories price:");


var balance = 1000;
var limit = 700;
const gSTtax = 0.18;
const mobilePrice = 300;
const accessoriesPrice = 70;
var accessoriesCount = 0;
var phonesCount = 0;
var purchaseAmount = 0;


function calGST(total) {
    return total + (total * gSTtax);
}


while ((purchaseAmount + calGST(mobilePrice) < limit)) {
    purchaseAmount += calGST(mobilePrice);
    phonesCount = phonesCount + 1;

    if (purchaseAmount + calGST(accessoriesPrice) < limit) {
        purchaseAmount += calGST(accessoriesPrice);
        accessoriesCount = accessoriesCount + 1;
    }
}

console.log("Bank Balance: " + balance);
console.log("Phones Purchased: " + phonesCount);
console.log("Accessories Purchased: " + accessoriesCount);
console.log("Total Purchase Amount: " + purchaseAmount.toFixed(2));
 balance -= purchaseAmount;
console.log("Ending Bank Balance: " + balance.toFixed(2));